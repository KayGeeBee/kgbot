package com.github.kgbot.main;

import org.pircbotx.Channel;
import org.pircbotx.Configuration;
import org.pircbotx.Configuration.Builder;
import org.pircbotx.PircBotX;
import org.pircbotx.hooks.ListenerAdapter;

import com.github.kgbot.listeners.CommandValidator;
import com.github.kgbot.listeners.InsultDisplay;
import com.github.kgbot.listeners.NoticeDisplay;
import com.github.kgbot.listeners.QueryRespond;
import com.github.kgbot.listeners.commands.Activate;
import com.github.kgbot.listeners.commands.Calculate;
import com.github.kgbot.listeners.commands.Casino;
import com.github.kgbot.listeners.commands.Colors;
import com.github.kgbot.listeners.commands.Deactivate;
import com.github.kgbot.listeners.commands.Disconnect;
import com.github.kgbot.listeners.commands.Help;
import com.github.kgbot.listeners.commands.Insults;
import com.github.kgbot.listeners.commands.Inventory;
import com.github.kgbot.listeners.commands.Links;
import com.github.kgbot.listeners.commands.Money;
import com.github.kgbot.listeners.commands.Poll;
import com.github.kgbot.listeners.commands.Quotes;
import com.github.kgbot.listeners.commands.Randomize;
import com.github.kgbot.listeners.commands.Raw;
import com.github.kgbot.listeners.commands.ServerStatus;
import com.github.kgbot.listeners.commands.Shop;
import com.github.kgbot.listeners.commands.Users;
import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.cron.Payday;
import com.github.kgbot.util.cron.RandomInsult;
import com.github.kgbot.util.cron.RandomQuote;
import com.github.kgbot.util.cron.Steal;
import com.github.kgbot.util.database.Database;

@SuppressWarnings("rawtypes")
public class KGBot
{
    private static PircBotX bot = null;

    // Constants
    public static final String BOT_PASSWORD = "KCKEjzY54vm";
    public static final String BOT_NAME = "KGBot";
    public static final String BOT_USER_NAME = "KGBot";    
    public static final String CHANNELS_MAIN = "#86683e042c764a6ee5d7e518931a4b67";
    public static final String NETWORK_NAME = "irc.rizon.net";
    public static final String LINE_STRING = "---------------------------------------------------";

    // Other Listeners
    public static CommandValidator commandValidator = new CommandValidator();
    public static NoticeDisplay noticeDisplay = new NoticeDisplay();
    public static QueryRespond queryRespond = new QueryRespond();
    public static InsultDisplay insultDisplay = new InsultDisplay();
    
    // Commands
    public static Calculate commandCalculate = new Calculate();
    public static Disconnect commandDisconnect = new Disconnect();
    public static Help commandHelp = new Help();
    public static Raw commandRaw = new Raw();
    public static Links commandLinks = new Links();
    public static Randomize commandRandomize = new Randomize();
    public static Quotes commandQuotes = new Quotes();
    public static Colors commandColors = new Colors();
    public static Activate commandActivate = new Activate();
    public static Deactivate commandDeactivate = new Deactivate();
    public static Users commandUsers = new Users();
    public static Casino commandCasino = new Casino();
    public static Money commandMoney = new Money();
    public static Poll commandPoll = new Poll();
    public static Inventory commandInventory = new Inventory();
    public static Shop commandShop = new Shop();
    public static ServerStatus commandServerStatus = new ServerStatus();
    public static Insults commandInsults = new Insults();
    
    private static BotCommand[] commands = { commandInsults, commandMoney, commandCasino, commandUsers, commandCalculate, commandDisconnect, commandHelp, commandRaw, commandLinks, commandRandomize, commandActivate, commandDeactivate, commandQuotes, commandColors, commandInventory, commandShop, commandServerStatus };
    private static ListenerAdapter[] otherListeners = { noticeDisplay, commandValidator, queryRespond, insultDisplay};
    private static String[] channels = { CHANNELS_MAIN };

    // Other
    private static boolean activated;

    public static Channel getMainChannel()
    {
        return bot.getUserChannelDao().getChannel(CHANNELS_MAIN);
    }

    public static PircBotX getBot()
    {
        return bot;
    }

    public static Channel[] getChannels()
    {
        Channel[] returnValue = new Channel[channels.length];
        
        for (int i = 0; i < returnValue.length; i++)
            returnValue[i] = bot.getUserChannelDao().getChannel(channels[i]);
        
        return returnValue;
    }

    public static BotCommand[] getCommands()
    {
        return commands;
    }

    public static boolean isActivated()
    {
        return activated;
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception
    {
        // Set general properties
        Builder config = new Configuration.Builder()
            .setName(BOT_NAME)
            .setMessageDelay(500L)
            .setLogin(BOT_USER_NAME)
            .setServerHostname(NETWORK_NAME);

        for (BotCommand command : commands)
        {
            config.addListener(command);
        }
        
        for (ListenerAdapter listener : otherListeners)
        {
            config.addListener(listener);
        }
        
        for (String channel : channels)
            config.addAutoJoinChannel(channel);
            
        bot = new PircBotX(config.buildConfiguration());
        bot.startBot();
    }

    public static void setActivated(boolean activated)
    {
        KGBot.activated = activated;
        RandomQuote.initialize();
        Payday.initialize();
        Steal.initialize();
        RandomInsult.initialize();
    }

    public static void disconnect()
    {
        try {
            bot.stopBotReconnect();
            Database.database.close();
            System.exit(0);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
