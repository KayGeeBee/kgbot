package com.github.kgbot.util;

import java.sql.ResultSet;

import com.github.kgbot.util.database.Database;
import com.github.kgbot.util.database.SQLValue;

public class Users
{
    public static boolean isUser(String user)
    {
        try {
            ResultSet rs = Database.executeQuery("SELECT username FROM " + Database.TABLE_USERS + " WHERE username = " + new SQLValue(user).getValue());
            boolean result = rs.first();
            
            return result;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
