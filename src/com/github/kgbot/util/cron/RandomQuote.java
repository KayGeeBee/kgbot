package com.github.kgbot.util.cron;

import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.pircbotx.Channel;

import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.database.Database;
import com.github.kgbot.util.database.SQLValue;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class RandomQuote
{
    private static class DisplayRandomQuotes
    {

        private static class LoopTask extends TimerTask
        {
            @Override
            public void run()
            {
                if (KGBot.isActivated())
                {
                    lastQuote = displayRandomQuote();
                }
            }
        }

        long delay = QUOTE_TIME;
        LoopTask task = new LoopTask();

        Timer timer = new Timer("DisplayRandomQuotes");

        public void start()
        {
            timer.cancel();
            timer = new Timer("DisplayRandomQuotes");
            Date executionDate = new Date(); // no params = now
            timer.scheduleAtFixedRate(task, executionDate, delay);
        }
    }

    private static final long QUOTE_TIME = 1000 * 60 * 30; // 30 mins
    private static String lastQuote = "";
    private static int lastQuoteNumber = 0;
    private static boolean initialized = false;
    
    private static HashMap<Integer, String> quotes = new HashMap<Integer, String>();

    public static int addQuote(String quote)
    {
        int id = quotes.size() + 1;
        quotes.put(id, quote);
        Database.execute("INSERT INTO " + Database.TABLE_QUOTES + " (id, quote) VALUES (" + id + ", " + new SQLValue(quote).getValue() + ")");
        return id;
    }

    public static String displayLastQuote(Channel channel)
    {
        Messages.sendMessage(channel, ColorFormat.QUOTE, "&b[" + lastQuoteNumber + "]&n '" + lastQuote + "'");
        return lastQuote;
    }

    public static String displayRandomQuote()
    {
        Random random = new Random(new Date().getTime());
        int randInt = random.nextInt(quotes.size());
        String newQuote = quotes.get(randInt);
        while (newQuote.equals(lastQuote))
        {
            randInt = random.nextInt(quotes.size());
            newQuote = quotes.get(randInt);
        }
        for (Channel channel : KGBot.getChannels())
        {
            if (channel.getName().equalsIgnoreCase("#86683e042c764a6ee5d7e518931a4b67"))
                Messages.sendMessage(channel, ColorFormat.QUOTE, "&b[" + (randInt) + "]&n '" + newQuote + "'");
        }
        return newQuote;
    }

    public static void editQuote(int oldQuoteId, String newQuote)
    {
        Database.execute("UPDATE " + Database.TABLE_QUOTES + " SET quote = " + new SQLValue(newQuote).getValue() + " WHERE id = " + oldQuoteId);
        quotes.put(oldQuoteId, newQuote);
    }

    public static HashMap<Integer, String> getQuotes()
    {
        return quotes;
    }

    public static void initialize()
    {
        if (!initialized)
        {
            try {
                ResultSet rs = Database.executeQuery("SELECT quote,id FROM " + Database.TABLE_QUOTES);
                
                quotes.clear();
                while (rs.next())
                {
                    quotes.put(rs.getInt("id"), rs.getString("quote"));   
                }
                
                DisplayRandomQuotes drq = new DisplayRandomQuotes();
                drq.start();
                
                initialized = true;
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void removeQuote(int quoteId)
    {
        Database.execute("DELETE FROM " + Database.TABLE_QUOTES + " WHERE id = " + quoteId);
        quotes.remove(quoteId);
    }
}
