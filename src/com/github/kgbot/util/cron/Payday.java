package com.github.kgbot.util.cron;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.pircbotx.User;

import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.MoneyMethods;
import com.github.kgbot.util.Users;
import com.github.kgbot.util.database.Database;
import com.github.kgbot.util.database.SQLValue;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class Payday
{
    public static final long PAYDAY_MINUTES = 120;
    public static final double PAYDAY_RATE = 2000;
    private static boolean initialized = false;
    
    private static class GivePay
    {
        private static class LoopTask extends TimerTask
        {
            @Override
            public void run()
            {
                for (User user : KGBot.getBot().getUserChannelDao().getUsers(KGBot.getMainChannel()))
                {
                    if (Users.isUser(user.getNick()))
                    {
                        Database.execute("UPDATE " + Database.TABLE_USERS + " SET money = " + new SQLValue(MoneyMethods.getMoney(user.getNick()) + PAYDAY_RATE).getValue() + " WHERE username = " + new SQLValue(user.getNick()).getValue());
                    }
                }
                Messages.sendMessage(KGBot.getMainChannel(), ColorFormat.NORMAL, "Payday! " +  MoneyMethods.format(PAYDAY_RATE) + " added to everyone's account.");
            }
        }
    
        long delay = 1000 * 60 * PAYDAY_MINUTES;
        LoopTask task = new LoopTask();
        Timer timer = new Timer("GivePay");
    
        public void start()
        {
            timer.cancel();
            timer = new Timer("GivePay");
            Date executionDate = new Date(); // no params = now
            timer.scheduleAtFixedRate(task, executionDate, delay);
        }
    }
    
    public static void initialize()
    {
        if (!initialized)
        {
            GivePay gp = new GivePay();
            gp.start();
            initialized = true;
        }
    }
}
