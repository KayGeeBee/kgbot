package com.github.kgbot.listeners;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.cron.RandomInsult;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class InsultDisplay extends ListenerAdapter<PircBotX>
{
    @Override
    public void onMessage(MessageEvent<PircBotX> event)
    {
        if (KGBot.isActivated())
        {
            if (Math.random() < RandomInsult.INSULT_CHANCE)
            {
                Messages.sendMessage(event.getChannel(), ColorFormat.INSULT, RandomInsult.getRandomInsult());
            }
        }
    }
}
