package com.github.kgbot.listeners.commands;

import java.util.HashMap;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.cron.ActualPoll;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Poll extends BotCommand
{
    public Poll()
    {
        getAliases().add("!poll");

        setDescription("Controls polls/voting");

        setArgumentsString("help|vote <poll id> <choice id>|new <question>|start <id> <duration> <choices>");
    }
    
    private static HashMap<Integer, String> polls = new HashMap<Integer, String>();
    private static HashMap<Integer, ActualPoll> actualPolls = new HashMap<Integer, ActualPoll>();

    public static HashMap<Integer, String> getPolls()
    {
        return polls;
    }

    public static HashMap<Integer, ActualPoll> getActualPolls()
    {
        return actualPolls;
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length >= 2)
            {
                switch (getArgs()[1])
                {
                    case "vote":
                        if (getArgs().length == 4)
                        {
                            if (NumberMethods.isInteger(getArgs()[2]) && NumberMethods.isInteger(getArgs()[3]))
                            {
                                int id = Integer.parseInt(getArgs()[2]);
                                ActualPoll poll = actualPolls.get(id);
                                
                                if (poll != null)
                                {
                                    int choiceId = Integer.parseInt(getArgs()[3]);
                                    
                                    if (poll.getChoices().size() > choiceId)
                                    {
                                        poll.vote(getUser().getNick(), choiceId);
                                        
                                        Messages.respond(getChannel(), ColorFormat.POLL, getUser(), "You have successfully voted on choice &b" + choiceId + "&n.");
                                    } else ErrorMessages.invalidPollChoice(getChannel(), getUser());
                                } else ErrorMessages.invalidPoll(getChannel(), getUser());
                            } else ErrorMessages.invalidNumber(getChannel(), getUser());
                        } else showUsage();
                        break;
                    case "start":
                        if (getArgs().length >= 5 && getArgs().length <= 14)
                        {
                            if (NumberMethods.isInteger(getArgs()[2]) && NumberMethods.isInteger(getArgs()[3]))
                            {
                                int id = Integer.parseInt(getArgs()[2]);
                                String question = polls.get(id);
                                
                                if (question != null)
                                {
                                    if (actualPolls.get(id) == null)
                                    {
                                        int duration = Integer.parseInt(getArgs()[3]);
                                        String choices = "";
                                        for (int i = 4; i < getArgs().length; i++)
                                        {
                                            choices += getArgs()[i] + " ";
                                        }
                                        choices = choices.substring(0, choices.length() - 1);
                                        
                                        actualPolls.put(id, new ActualPoll(id, duration, question, choices, getChannel()));
                                        
                                        String[] splitChoices = choices.split(" ");
                                        
                                        Messages.sendMessage(getChannel(), ColorFormat.POLL, "Poll ID &b" + id + "&n started. Duration: &b" + duration + " minutes&n. Choices:");   
                                        for (int i = 0; i < splitChoices.length; i++)
                                        {
                                            Messages.sendMessage(getChannel(), ColorFormat.POLL, "[" + i + "] " + splitChoices[i]);   
                                        }
                                    } else ErrorMessages.pollAlreadyStarted(getChannel(), getUser());
                                } else ErrorMessages.invalidPoll(getChannel(), getUser());
                            } else ErrorMessages.invalidNumber(getChannel(), getUser());
                        } else showUsage();
                        break;
                    case "new":
                        if (getArgs().length >= 3)
                        {
                            String question = "";
                            for (int i = 2; i < getArgs().length; i++)
                            {
                                question += getArgs()[i] + " ";
                            }
                            question = question.substring(0, question.length() - 1);
                            
                            int id = polls.size();
                            
                            polls.put(id, question);
                            
                            Messages.sendMessage(getChannel(), ColorFormat.POLL, "Poll created with the ID of &b" + id + "&n. Start it with &b!poll start " + id + " <duration in mins> <choices>");      
                        } else showUsage();
                        break;
                    case "help":
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!poll commands list:");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!poll help: - Displays this message.");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!poll new <question>: - Prepares a new poll.");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!poll start <id> <duration> <choices>: - Starts a prepared poll.");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!poll vote <poll id> <choice id>: - Votes on a poll.");
                        break;
                    default: 
                        showUsage(); 
                        break;
                }
            }
        }
    }
}
