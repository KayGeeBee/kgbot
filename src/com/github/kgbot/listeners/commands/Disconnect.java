package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class Disconnect extends BotCommand
{
    public Disconnect()
    {
        getAliases().add("!disconnect");
        getAliases().add("!gtfo");

        setDescription("Disconnects the bot from the network");

        setArgumentsString("");
        
        setWorksWhenDeactivated(true);
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "KGBot feels useless. KGBot is sad. KGBot is disconnecting ;(");
                KGBot.disconnect();
            } else showUsage();
        }
    }
}
