package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class Raw extends BotCommand
{
    public Raw()
    {
        getAliases().add("!raw");
        getAliases().add("!sendraw");

        setDescription("Sends a raw message to the IRC server");

        setArgumentsString("<raw line to send to server>");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length >= 2)
            {
                String toSend = "";
                for (int i = 1; i < getArgs().length; i++)
                {
                    toSend += getArgs()[i] + " ";
                }
                KGBot.getBot().sendRaw().rawLine(toSend.substring(0, toSend.length() - 1));
                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Sending raw message to server: &b" + toSend);
            } else showUsage();
        }
    }
}
