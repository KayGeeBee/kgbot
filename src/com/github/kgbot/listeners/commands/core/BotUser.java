package com.github.kgbot.listeners.commands.core;

import org.pircbotx.User;

public class BotUser {
    public BotUser(String nick) {
        this.nick = nick;
    }
    
    public BotUser(User user) {
       this.ircUser = user;
    }
    
    private String nick;
    private User ircUser;
    
    public String getNick() {
        return ircUser == null ? nick : ircUser.getNick();
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public User getIrcUser() {
        return ircUser;
    }

    public void setIrcUser(User ircUser) {
        this.ircUser = ircUser;
    }
    
}
