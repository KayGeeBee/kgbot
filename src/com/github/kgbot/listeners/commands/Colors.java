package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class Colors extends BotCommand
{
    public Colors()
    {
        getAliases().add("!colors");
        getAliases().add("!color");
        getAliases().add("!rainbow");

        setDescription("Displays a list of colors");

        setArgumentsString("(text to test)");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "&bList of colors:&n &bb&n &uu&n &11&n &22&n &33&n &44&n &55&n &66&n &77&n &88&n &99&n &1010&n &1111&n &1212&n &1313&n &1414&n &1515&n &1616&n");
                Messages.respondRaw(getChannel(), ColorFormat.NORMAL, getUser(), "Use &r to specify a background color. Use &n to get rid of all formatting (except the default one).");
                Messages.respondRaw(getChannel(), ColorFormat.NORMAL, getUser(), "Example: &1&r&4&b&u will make red, bolded, underlined text on white background.");
                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "You can test colors with &b!colors <text to test>");
            } else if (getArgs().length >= 2)
            {
                String toTest = "";
                for (int i = 1; i < getArgs().length; i++)
                {
                    toTest += getArgs()[i] + " ";
                }
                toTest = toTest.substring(0, toTest.length());
                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "&bTesting colors:&n " + toTest);
            } else showUsage();
        }
    }

    /*
     * NORMAL &n BOLD &b UNDERLINE &u REVERSE &r
     * 
     * WHITE &1 BLACK &2 DARK_BLUE &3 DARK_GREEN &4 RED &5 BROWN &6 PURPLE &7 OLIVE &8 YELLOW &9 GREEN &10 TEAL &11 CYAN &12 BLUE &13 MAGENTA &14 DARK_GRAY &15 LIGHT_GRAY &16
     */
}
