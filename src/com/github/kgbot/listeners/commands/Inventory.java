package com.github.kgbot.listeners.commands;

import java.util.Map.Entry;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.listeners.commands.core.BotUser;
import com.github.kgbot.util.Items;
import com.github.kgbot.util.UserInventory;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class Inventory extends BotCommand
{
    public Inventory()
    {
        getAliases().add("!inventory");
        getAliases().add("!bag");

        setDescription("Controls your inventory");

        setArgumentsString("view");
    }
    
    public void view(BotUser user) throws Exception
    {
        UserInventory inventory = UserInventory.getInventory(user.getNick());
        
        Messages.sendNotice(ColorFormat.NORMAL, user, "Your inventory:");
        int i = 1;
        for (Entry<Integer, Integer> entry : inventory.getInventory().entrySet())
        {
            Messages.sendNotice(ColorFormat.NORMAL, user, i + ". " + Items.idToItem(entry.getKey()) + " " + entry.getValue() + "x");
            i++;
        }
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length >= 2)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "view":
                        view(getUser());
                        break;
                    default: 
                        showUsage();
                        break;
                }
            } else
            {
                view(getUser());
            }
        }
    }
}
