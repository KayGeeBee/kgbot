package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Deactivate extends BotCommand
{
    public Deactivate()
    {
        getAliases().add("!deactivate");
        getAliases().add("!off");
        getAliases().add("!stfu");

        setDescription("Deactivates the bot");

        setArgumentsString("");

        setWorksWhenDeactivated(true);
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                if (KGBot.isActivated())
                {
                    Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "Nooooooooooooooooooooooooooooooooooo! :( Bot deactivated.");
                    KGBot.setActivated(false);
                } else ErrorMessages.alreadyDeactivated(getChannel(), getUser());
            } else showUsage();
        }
    }
}
