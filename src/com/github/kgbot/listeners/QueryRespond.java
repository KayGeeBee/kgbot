package com.github.kgbot.listeners;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.listeners.commands.core.BotUser;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.messages.ErrorMessages;

public class QueryRespond extends ListenerAdapter<PircBotX>
{
    @Override
    public void onPrivateMessage(PrivateMessageEvent<PircBotX> event) throws Exception
    {
        String[] split = event.getMessage().split(" ");
        for (BotCommand command : KGBot.getCommands())
        {
            if (command.isAlias(split[0]))
            {
                command.onMessage(new MessageEvent<PircBotX>(KGBot.getBot(), null, event.getUser(), event.getMessage()));
                return;
            }
        }
        ErrorMessages.notCommand(null, new BotUser(event.getUser()), split[0]);
    }
}
